terraform {
  required_providers {
    azurerm = {
      source                = "hashicorp/azurerm"
      configuration_aliases = [azurerm.vnet1, azurerm.vnet2]
    }
  }
}

resource "azurerm_virtual_network_peering" "peer" {
  name                      = "${var.vnet1.network_name}-to-${var.vnet2.network_name}"
  resource_group_name       = var.vnet1.resource_group_name
  virtual_network_name      = var.vnet1.network_name
  remote_virtual_network_id = var.vnet2.network_id
  allow_gateway_transit     = true
}

resource "azurerm_virtual_network_peering" "peer2" {
  provider                  = azurerm.vnet2
  name                      = "${var.vnet2.network_name}-to-${var.vnet1.network_name}"
  resource_group_name       = var.vnet2.resource_group_name
  virtual_network_name      = var.vnet2.network_name
  remote_virtual_network_id = var.vnet1.network_id
  use_remote_gateways       = true
}

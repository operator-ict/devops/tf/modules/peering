variable "vnet1" {
  type = object({
    network_name        = string
    resource_group_name = string
    network_id          = string
  })
}

variable "vnet2" {
  type = object({
    network_name        = string
    resource_group_name = string
    network_id          = string
  })
}
